<?php
/*
n! = n x (n-1)!

5! = 5 x 4!
4! = 4 x 3!;
3! = 3 x 2 x 1;
2! = 2 x 1;
1! = 1;
*/
function factorial($n) {
    echo "Calculating $n<br />";
    if($n == 1) return 1;
    
    return $n * factorial($n-1);
}

echo factorial(10)."<br />";
//echo factorial(20);