<?php
echo "<pre>";
$arr = range(1,10);

print_r($arr);

$arr[4] = '';

unset($arr[5]);

print_r($arr);

for($i = 0;$i<count($arr);$i++) {
    echo $i." > ".$arr[$i]."<br />";
}

$twoD = [];

$twoD[] = range(1,10);
$twoD[] = range(11,20);
$twoD[] = range(21,30);

print_r($twoD);

for($i = 0;$i<count($twoD);$i++) {
    for($j = 0; $j < count($twoD[$i]);$j++)
      echo "[".$i."][".$j."] = ".$twoD[$i][$j]."<br />";
}

foreach($twoD as $i => $row) {
    foreach($row as $j => $val)
      echo "[".$i."][".$j."] = ".$val."<br />";
}