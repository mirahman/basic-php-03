<?php

class Car  {
    
    public function myName() {
        //self::getName();
        $this->getName();
    }
    
    
    public function getName() {
        echo " i am Car <br />";
    }
}

class Bus extends Car {
        
    public function getName() {
        echo " i am Bus <br />";
    }
}


$cric = new Bus();
$cric->myName();

$abc = new Car();
$abc->myName();
