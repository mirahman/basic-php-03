<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Car {
    
    public $name = "Toyota";
    
}

$a = new Car();
$b = &$a;

echo $a->name."<br />";
echo $b->name."<br />";

$b->name = "Ferrari";

echo $a->name."<br />";
echo $b->name."<br />";

$a = null;

echo $a->name."<br />";
echo $b->name."<br />";
